package studentCoursesBackup.myTree;

import java.util.ArrayList;
import java.util.Collections;

import studentCoursesBackup.myTree.ObserverI;
import studentCoursesBackup.myTree.SubjectI;
import studentCoursesBackup.util.TreeBuilder;
/**
 * Java Class implements the SubjectI,ObjectI and Cloneable Interfaces and
 * and implements the methods in interfaces and contains getters and setters
 * for the ArrayList and private fields and also overrides the clone method.
 * 
 * @author kdamera1
 */
public class Node implements  SubjectI,ObserverI,Cloneable {
	private ArrayList<Node> allObservers = new ArrayList<>();
    public ArrayList<Node> getAllObservers() {
		return allObservers;
	}
	public void setAllObservers(ArrayList<Node> allObservers) {
		this.allObservers = allObservers;
	}
	private int bnumber;
    private Node left;
    private Node right;
    ArrayList<String> alist = new ArrayList<String>();
    public void setarraylist(String s){
		alist.add(s);
	}
    public boolean myStrings(String s){  //Method to tell whether the same course exists in the ArrayList or not.
    	boolean flag = false;
    	for(int i=0;i<alist.size();i++)
    	{
    		if(s.equals(alist.get(i))){
    			flag = true;
    			break;
    		}
    	}
    	return flag;
	}
    public void myStrings1(String s){   //Method to remove course from a arraylist
       for(int i=0;i<alist.size();i++)
    	{
    		if(s.equals(alist.get(i))){
    			alist.remove(i);
    			break;
    		}
    	}
	}  
    public ArrayList<String> getStrings(){
    	return alist;
    }
    public Node(int num) {
    	this.bnumber = num;
    }
	public void setbnumber(int number){
		bnumber = number;
	}
	public int getbnumber(){
		return bnumber;
	}
	
	public Node getLeft() {
		return left;
	}
	public Node getRight() {
		return right;
	}
	public void setLeft(Node left) {
		this.left = left;
	}
	public void setRight(Node right) {
		this.right = right;
	}
	public String toString(){
		return "BingId Number is: "+bnumber;
	}
    public void registerObserver(Node o)
  	{
  		allObservers.add(o);
  	}	
  	public void notifyAll(Node r, int num)       //Method to Notify all the Observers
  	{
  		for(int i=0; i<allObservers.size(); i++)
  		{
  			ObserverI o = (ObserverI)allObservers.get(i);
  			o.update(alist);
  		}
  	}
    public void update(ArrayList al)
	{
    	this.alist = al;
	}
    public Object clone() throws CloneNotSupportedException{
    	if(this instanceof Cloneable) {
    		Node n = new Node(this.bnumber);
    		return n;
    	}
    	else throw new CloneNotSupportedException(getClass().getName());
    	
    }
}
