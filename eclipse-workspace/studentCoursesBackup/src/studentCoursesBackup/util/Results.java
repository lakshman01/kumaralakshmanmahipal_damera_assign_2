package studentCoursesBackup.util;

import java.io.BufferedWriter;
import java.util.ArrayList;
import studentCoursesBackup.myTree.Node;
/**
 * Results Class to store the Bnumber and the arraylist and passing it to write to a file and
 * it implements the FileDisplayInterface and contains method to print the nodes 
 * following the Inorder Traversal
 * @author kdamera1
 *
 */
public class Results implements FileDisplayInterface {
    private FileProcessor fp;
    String s, res,s1;
    public ArrayList<String> alist1 = new ArrayList<String>();
  public void writeToFile(String s,BufferedWriter writer) {
			fp.writeToFile1(s,writer);
   }
	
  public Results(FileProcessor f) {
		   fp= f;
	}  
	 
  public void printNodes(Node tempNode , BufferedWriter writer)
			{
				if(tempNode!=null)
				{   
					StringBuffer s = new StringBuffer();
					printNodes(tempNode.getLeft(),writer);
				    alist1 = tempNode.getStrings();
				    for(int i=0;i<alist1.size();i++) {
						 s.append(alist1.get(i).concat(" "));
				    }
				    s1 = Integer.toString(tempNode.getbnumber());
				    res = s1+":"+s;
				    writeToFile(res,writer);
					printNodes(tempNode.getRight(),writer);
					
				}
			}
}