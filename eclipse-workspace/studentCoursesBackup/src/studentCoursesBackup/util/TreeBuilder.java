package studentCoursesBackup.util;

import studentCoursesBackup.myTree.Node;

public class TreeBuilder {   		
    public Node root; 
 
    public Node searchNode(int num)
		{   
			Node tempNode = root;
			if (tempNode==null)
		        return tempNode;
			while(tempNode.getbnumber() != num)
			{
				if(num < tempNode.getbnumber()) {
					tempNode = tempNode.getLeft();
				     if(tempNode==null)
				    	break;
				}
				else {
					tempNode = tempNode.getRight();
					if(tempNode == null)
						break;
					}
			}
		return tempNode;
		}  
    public void insert(int num)
	{
		Node temp, tempNode, temp1Node;
		temp = new Node(num);
		try
		{
			if(root == null) {
				root = temp;
			}
			else
			{  
				tempNode = root;
				while(true)
				{
					temp1Node = tempNode;
					if(num > tempNode.getbnumber())
					{
						tempNode = tempNode.getRight();
						if(tempNode==null)
						{
							temp1Node.setRight(temp);
							return;
						}
					}
					else
					{
						tempNode = tempNode.getLeft();
						if(tempNode==null)
						{
							temp1Node.setLeft(temp);
							return;
						}
					}
				}
			}
		}
		catch(Exception e)
		{
			System.out.println("Exception occured: "+e.getMessage());
			System.exit(0);
		}
	}
}

