package studentCoursesBackup.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;

public class FileProcessor {
	  private BufferedReader breader;
      private BufferedWriter bwriter;
      private BufferedReader breader1;
      private BufferedWriter bwriter1;
      private BufferedWriter bwriter2;
      private String IfileName;
      private String DfileName;
      private String O1fileName;
      private String O2fileName;
      private String O3filename;
public FileProcessor(String inputfname, String delfname,String o1fname,String o2fname, String o3fname) { //Constructor to accept command line arguments from Driver.java
	   System.out.println("Filename is:"+inputfname);
	   this.IfileName  = inputfname;
	   this.DfileName = delfname;
	   this.O1fileName = o1fname;
	   this.O2fileName = o2fname;
	   this.O3filename = o3fname;
	   this.initReader();
	   this.initWriter();
  }
public void initReader() {      //Method to initialize the filereader method 
	  try {
		  FileReader fr = new FileReader(IfileName);
		  FileReader fr1 = new FileReader(DfileName);
		  breader = new BufferedReader(fr);
		  breader1 = new BufferedReader(fr1);
                   }
	  catch(Exception e) {
		  e.printStackTrace();
	  }
  }
public void initWriter() {   //method to initialise the filewriter method
	  try {
	  FileWriter fw = new FileWriter(O1fileName);
	  FileWriter fw2 = new FileWriter(O2fileName);
	  FileWriter fw3 = new FileWriter(O3filename);
	  bwriter = new BufferedWriter(fw);
	  bwriter1 = new BufferedWriter(fw2);
	  bwriter2 = new BufferedWriter(fw3);
	  
  }
	  catch(Exception e) {
		  e.printStackTrace();
	  }
  }
public String Return() throws Exception {   //method for reading line by line from the  inputfile
       String input = null;
        try {
        	input = breader.readLine();
        }
        catch(Exception e) {
        	e.printStackTrace();
        }
    return input;
    }
public String Return1() throws Exception {   //method for reading line by line from the  deletefile
    String input = null;
     try {
     	input = breader1.readLine();
     }
     catch(Exception e) {
     	e.printStackTrace();
     }
 return input;
 }
public void writeToFile1(String s) {   //method for writing to file 
	  try {
		  bwriter.write(s);
		  bwriter.write("\n");
		  bwriter.flush();
	  }
	  catch(Exception e) {
		  e.printStackTrace();
	  }
  }
public void writeToFile2(String s) {   //method for writing to file 
	  try {
		  bwriter1.write(s);
		  bwriter1.write("\n");
		  bwriter1.flush();
	  }
	  catch(Exception e) {
		  e.printStackTrace();
	  }
}
public void writeToFile3(String s) {   //method for writing to file 
	  try {
		  bwriter2.write(s);
		  bwriter2.write("\n");
		  bwriter2.flush();
	  }
	  catch(Exception e) {
		  e.printStackTrace();
	  }
}
}


