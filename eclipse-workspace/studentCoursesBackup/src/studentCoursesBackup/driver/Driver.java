package studentCoursesBackup.driver;

import studentCoursesBackup.myTree.Node;
import studentCoursesBackup.util.FileProcessor;
import studentCoursesBackup.util.Results;
import studentCoursesBackup.util.TreeBuilder;
/**
 * Main Class From where Execution starts which checks for arguments validity and
 * implements prototype pattern which takes input from one file and
 * updates the tree with another file and finally using the results instances 
 * prints the Nodes to the corresponding 3 output files.
 * */
public class Driver {
    public static boolean flag=false;
	public static void main(String[] args) throws CloneNotSupportedException {
	    Driver d = new Driver();
	    d.argumentsCheck(args);
            TreeBuilder p1 = null;
	    TreeBuilder p2 = null;
	    TreeBuilder p3 = null;
	    String s = null;
	    String txt = null;
	    int ret =0;
	    Node node_orig = null; 
	    Node backup_Node_1 = null;
	    Node backup_Node_2 = null;
	    Node ne,no;
	    Node n1 = null,n2 = null,n3 = null;
     try{
	       FileProcessor fp = new FileProcessor(args[0], args[1], args[2], args[3], args[4]);
	       Results r1 = new Results(fp);
	       Results r2 = new Results(fp);
	       Results r3 = new Results(fp);
               while((s=fp.Return())!=null){   //Block to read a file line by line and build a tree by implementing prototype Pattern
                     String[] words = new String[10]; 
               	     words = s.split(":");
               	     int j = Integer.parseInt(words[0]);
                	   if (ret ==0) {
                		     node_orig = new Node(Integer.parseInt(words[0]));
                		     backup_Node_1 = (Node)node_orig.clone();
                		     backup_Node_2 = (Node)node_orig.clone();
                		     node_orig.registerObserver(backup_Node_1);
                                     node_orig.registerObserver(backup_Node_2);
                                     p1 = new TreeBuilder(node_orig);
                                     p2 = new TreeBuilder(backup_Node_1); 
                                     p3 = new TreeBuilder(backup_Node_2);
                                     node_orig.getStrings().add(words[1]);
                                     backup_Node_1.getStrings().add(words[1]);
                                     backup_Node_2.getStrings().add(words[1]);
                		     ret = 1;
                		     continue;
                	  }
                	  ne = p1.searchNode(j);
                	  if(ne == null) {
                	  n1 = new Node(Integer.parseInt(words[0]));
                	  n2 = (Node)n1.clone();
                	  n3 = (Node)n1.clone();
                	  n1.registerObserver(n2);
                	  n1.registerObserver(n3);
                	  p1.insert(n1);
                          p2.insert(n2);
                          p3.insert(n3);
                          n1.getStrings().add(words[1]);
                          n2.getStrings().add(words[1]);
                          n3.getStrings().add(words[1]);
                	   }else {
                		   boolean b = ne.myStrings(words[1]);
                		   if(b) {
                		   }
                		   else {
                			   ne.getStrings().add(words[1]);
                			   ne.getAllObservers().get(0).getStrings().add(words[1]);
                			   ne.getAllObservers().get(1).getStrings().add(words[1]);
                		   }
                		  
                	   }
                	  
                 }
                 while((txt = fp.Return1())!=null) {         //Block for reading input from second file and updating the tree
                	 String[] words1 = new String[10];
                	 words1 = txt.split(":");
                	 no = p1.searchNode(Integer.parseInt(words1[0]));
                  	if(no!=null) {
              		     boolean b1 = no.myStrings(words1[1]);
                		if(b1) {
               			    no.myStrings1(words1[1]);
           			    no.notifyAll(no, Integer.parseInt(words1[0]));
                		}
                	}
                  }
                      r1.printNodes(node_orig,fp.getBwriter());
                      r2.printNodes(backup_Node_1,fp.getBwriter1());
                      r3.printNodes(backup_Node_2,fp.getBwriter2());
           }
         catch(NumberFormatException e){
              System.err.println("Given Input is not a Number"+e);
              System.exit(1);
         }
	 catch(ArrayIndexOutOfBoundsException e){
             System.err.println("Exception Found"+e);
             System.exit(1);
	 }
         catch(Exception e) {
        	System.exit(1);
         }
  }
  public void argumentsCheck(String[] args){   //Method to Check For validity of Arguments
	            boolean flag = false;
            if(args.length!=5){
            System.err.println("Arguments Parameters must be exactly 5");
            System.exit(1);
	    }
     }
}
